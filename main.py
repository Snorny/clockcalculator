



def getValue():

    data = [0, 0, 0, 0]

    data[0] = int(input("Frequency Timer desired (Hz):"))
    data[1] = int(input("Number of bits in the CNT register:"))
    data[2] = int(input("Number of bits in the PSC register:"))
    data[3] = int(input("Frequency of the timer clock (MHz):"))

    return data
    



if __name__ == "__main__" :  
    data = getValue()
    print(data)

